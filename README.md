# About

This repo provides a ready to use docker image with the patched version of [Commento](https://gitlab.com/commento/commento) using the [Commento++](https://github.com/souramoo/commentoplusplus) repository over at github.
You can see that the Gitlab runner produces the image using the repository's own build file.
I did not configure any webhooks so if you are missing a version you can ping me about this by creating a new issue on here.

All credits for Commento go to the respective software's author and fork maintainers.
**I do not take any responsibility for anything.**

All images build here are mirrored to [https://hub.docker.com/r/caroga/commentoplusplus](https://hub.docker.com/r/caroga/commentoplusplus)

## Examples

docker-compose.yml

```yaml
version: '3.7'

services:
  commento:
    image: caroga/commentoplusplus:v1.8.7
    environment:
      COMMENTO_ORIGIN: 'https://your.domain.com'
      COMMENTO_PORT: '8080'
      COMMENTO_POSTGRES: 'postgres://username:password@db:5432/commento?sslmode=disable'
      #COMMENTO_FORBID_NEW_OWNERS: 'true' # enable this after your own registration
      COMMENTO_GZIP_STATIC: 'true'
    depends_on:
      - db
    ports:
      - "8080/8080"

  db:
    image: postgres:12.5-alpine
    environment:
      POSTGRES_DB: 'commento'
      POSTGRES_USER: 'username'
      POSTGRES_PASSWORD: 'password'

```
